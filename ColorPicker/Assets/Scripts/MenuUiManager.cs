﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUiManager : MonoBehaviour {
    public Sprite OffSprite;
    public Sprite OnSprite;
    public Button but;
    public GameObject ScorePanel;
    // Use this for initialization
    void Start () {
		
	}
    public void Musiconoff()
    {
        if (PlayerPrefs.GetInt("music") == 0)
        {
            but.image.sprite = OffSprite;
            AudioListener.pause = true;
            PlayerPrefs.SetInt("music", 1);
            PlayerPrefs.Save();
        }
        else if (PlayerPrefs.GetInt("music") == 1)
        {
            but.image.sprite = OnSprite;
            AudioListener.pause = false;
            PlayerPrefs.SetInt("music", 0);
            PlayerPrefs.Save();
        }
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKey("escape"))
            Application.Quit();
    }
    public void PLay()
    {
        SceneManager.LoadScene("Level1");
    }
    public void ScoreButton()
    {
        ScorePanel.SetActive(true);
    }
    public void BackButton()
    {
        ScorePanel.SetActive(false);
    }
}
