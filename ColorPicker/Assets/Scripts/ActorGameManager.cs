﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActorGameManager : MonoBehaviour
{

    // Use this for initialization
    bool started;
    bool gameOver;
    float timeLeft = 10.0f;
    public Text timeText;
    public AudioClip[] audioClip;
    //  Drag and drop the single circle here
    [SerializeField]
    GameObject singleCircle;
    //  Drag and drop the four circles in this table
    [SerializeField]
    GameObject[] multipleCircles = new GameObject[4];
    //  The list of color
    public Color[] colors;


    //  Start :
    //      Assign a new color to every circle at the start
    void Start()
    {
        started = false;
        gameOver = false;
    }
    //  SetColor() :
    //      Change the color of all five circles
    void SetColor()
    {
        int random;
        Color tempColor;

        //  Changing the single circle color
        singleCircle.GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Length)];

        //  Changing all circle colors
        for (int i = 0; i < 4; i++)
        {
            random = Random.Range(i, 4);
            multipleCircles[i].GetComponent<SpriteRenderer>().material.color = colors[random];
            tempColor = colors[i];
            colors[i] = colors[random];
            colors[random] = tempColor;
        }
    }
    void SetLevel()
    {
        if (ScoreManager.instance.score <= 5)
        {
            InvokeRepeating("SetColor", 0.0f, 3.0f);

        }
        else if (ScoreManager.instance.score > 5 && ScoreManager.instance.score <= 10)
        {
            InvokeRepeating("SetColor", 0.0f, 2.5f);

        }
        else if (ScoreManager.instance.score > 10 && ScoreManager.instance.score <= 15)
        {
            InvokeRepeating("SetColor", 0.0f, 2.0f);

        }
        else if (ScoreManager.instance.score > 15 && ScoreManager.instance.score <= 20)
        {
            InvokeRepeating("SetColor", 0.0f, 1.5f);

        }
        else if (ScoreManager.instance.score > 20 && ScoreManager.instance.score <= 30)
        {
            InvokeRepeating("SetColor", 0.0f, 1.0f);

        }
        else if (ScoreManager.instance.score > 30 && ScoreManager.instance.score <= 200)
        {
            InvokeRepeating("SetColor", 0.0f, 0.8f);
        }
        else
        {
            InvokeRepeating("SetColor", 0.0f, 0.5f);

        }

    }
    void playSound(int clip)
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.clip = audioClip[clip];
        audio.Play();
    }

    //  CheckAndUpdateColor :
    //      Check if the given color is the same as the single circle
    //      Then update all colors
    public void CheckAndUpdateColor(Color color)
    {
        if (color == singleCircle.GetComponent<Renderer>().material.color && !gameOver)
        {
            ScoreManager.instance.IncrementScore();
            timeLeft = 10.0f;
            playSound(0);
            SetColor();
            CancelInvoke("SetColor");
            StartCoroutine("SetLevel");





        }
        else
        {
            playSound(1);
            gameOver = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!started)
        {
            started = true;
            InvokeRepeating("SetColor", 0.0f, 2.0f);

        }
        if (started == true)
        {
            timeLeft = timeLeft - Time.deltaTime;
            timeText.text = "Time Left: " + Mathf.Round(timeLeft);
            if (timeLeft < 0)
            {
                gameOver = true;

            }
        }
        if (gameOver == true)
        {
            GameManager.instance.GameOver();
            StopCoroutine("SetLevel");
            timeLeft = 0;
        }
    }
}
