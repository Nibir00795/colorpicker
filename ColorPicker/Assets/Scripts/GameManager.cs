﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    bool gameOver;



    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
            SceneManager.LoadScene("Menu");
    }



    public void GameOver()
    {
        gameOver = true;
        ScoreManager.instance.StopScore();
        UiManager.instance.GameOver();

    }
}
