﻿using UnityEngine;
using System.Collections;

public class ActorCircle : MonoBehaviour
{

    // Use this for initialization
    public Transform center;
    private float degreesPerSecond = -65.0f;
    private Vector3 v;
    public static ActorCircle instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    //  Drag and drop the item which holds the ActorGameManager script
    [SerializeField]
    GameObject gameManager;

    //  Local variable for the script and the material of this gameObject
    ActorGameManager actorGameManager;
    Material material;

    //  Start :
    //      Set the local variable at the start
    void Start()
    {
        v = transform.position - center.position;
        actorGameManager = gameManager.GetComponent<ActorGameManager>();
        material = gameObject.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (ScoreManager.instance.score <= 5)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime, Vector3.forward) * v;

        }
        else if (ScoreManager.instance.score > 5 && ScoreManager.instance.score <= 10)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 1.5f, Vector3.forward) * v;

        }
        else if (ScoreManager.instance.score > 10 && ScoreManager.instance.score <= 15)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 2.0f, Vector3.forward) * v;

        }
        else if (ScoreManager.instance.score > 15 && ScoreManager.instance.score <= 20)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 2.5f, Vector3.forward) * v;

        }
        else if (ScoreManager.instance.score > 20 && ScoreManager.instance.score <= 30)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 3.0f, Vector3.forward) * v;

        }
        else if (ScoreManager.instance.score > 30 && ScoreManager.instance.score <= 200)
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 4.5f, Vector3.forward) * v;
        }
        else
        {
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * 15, Vector3.forward) * v;

        }
        
        transform.position = center.position + v;
    }

    void OnMouseDown()
    {
        actorGameManager.CheckAndUpdateColor(material.color);
    }
}
